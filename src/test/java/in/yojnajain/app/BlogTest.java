package in.yojnajain.app;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BlogTest {
	@Test
	public void test() {
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://twhyderabad.github.io/demo_site/index.html");
		LoginService loginService = new LoginService();
		BlogService blogService = new BlogService();
		System.out.println("Hello world");
		loginService.login(driver, "admin", "admin");
		driver.get(driver.getCurrentUrl());
		blogService.createBlog(driver, "ABC XYZ", "56", 8,
				"This is a blog area.", "doctor");
		blogService.verifyBlog(driver, "ABC XYZ", "56", 8,
				"This is a blog area.", "doctor");
		loginService.logout(driver);
		driver.quit();

	}

}
