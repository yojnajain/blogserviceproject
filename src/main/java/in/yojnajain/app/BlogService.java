package in.yojnajain.app;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BlogService {

	public int createBlog(WebDriver driver, String name, String age,
			int experience, String blogText, String profession) {
		System.out.println("---- Create New Blog Test Case ----");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.id("name")));

			driver.findElement(By.id("name")).sendKeys(name);
			driver.findElement(By.id("age")).sendKeys(age);
			driver.findElement(By.id("blogArea")).sendKeys(blogText);
			// driver.findElement(By.id("profession")).sendKeys();
			int exp = 0;
			if (experience <= 3) {
				exp = 0;
			} else if (experience <= 7) {
				exp = 1;
			} else if (experience <= 15) {
				exp = 2;
			} else {
				exp = 3;
			}
			driver.findElements(By.name("experience")).get(exp).click();
			Select dropdown = new Select(
					driver.findElement(By.id("profession")));
			if (profession.equalsIgnoreCase("Enginner")) {
				dropdown.selectByVisibleText("Engineer");
			} else if (profession.equalsIgnoreCase("Doctor")) {
				dropdown.selectByVisibleText("Doctor");
			} else if (profession.equalsIgnoreCase("Teacher")) {
				dropdown.selectByVisibleText("Teacher");
			} else if (profession.equalsIgnoreCase("Business")) {
				dropdown.selectByVisibleText("Business");
			} else {
				dropdown.selectByVisibleText("other");
				driver.findElement(By.id("otherProfession")).sendKeys(
						profession);
			}

			driver.findElement(By.xpath("//button[contains(text(), 'Save')]"))
					.click();
			System.out.println("Blog created successfully.");
			return 1;
		} catch (TimeoutException e) {
			System.out.println("ERROR: Blog form is not visible properly.");
			e.printStackTrace();
		} catch (NoSuchElementException e) {
			System.out.println("ERROR: Blog form is not visible properly.");
			e.printStackTrace();
		}
		System.out.println("ERROR: Error in creation of blog.");
		return 0;
	}

	public int verifyBlog(WebDriver driver, String name, String age,
			int experience, String blogText, String profession) {
		System.out.println("---- Read BLog Test Case ----");
		driver.findElement(By.linkText("Blogs")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.textToBePresentInElementLocated(
					By.xpath("html/body/div/h1"), "Blogs"));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(".//*[@id='collapse0']/div/span[1]")));
			String b_name = driver.findElement(
					By.xpath(".//*[@id='collapse0']/div/span[1]")).getText();
			if (b_name.equals(name)) {
				System.out.println("Name is same as given.");
			} else {
				System.out.println("Name is not same as given.");
			}
			String b_age = driver.findElement(
					By.xpath(".//*[@id='collapse0']/div/span[2]")).getText();
			if (b_age.equals("" + age)) {
				System.out.println("Age is same as given.");
			} else {
				System.out.println("Age is not same as given.");
			}

			String b_experience = driver.findElement(
					By.xpath(".//*[@id='collapse0']/div/span[3]")).getText();
			if (experience <= 3 && b_experience.equals("0-3")) {
				System.out.println("Experience is same as given.");
			} else if (experience > 3 && experience <= 7
					&& b_experience.equals("3-7")) {
				System.out.println("Experience is same as given.");
			} else if (experience > 7 && experience <= 15
					&& b_experience.equals("7-15")) {
				System.out.println("Experience is same as given.");
			} else if (experience > 15 && b_experience.equals("15+")) {
				System.out.println("Experience is same as given.");
			} else if ((b_experience == null && experience < 0)
					|| b_experience.equals("undefined")) {
				System.out
						.println("Given Experience by user has format problem.");
			} else {
				System.out.println("Experience is not same as given.");
			}
			String b_blog = driver.findElement(
					By.xpath(".//*[@id='collapse0']/div/span[4]")).getText();
			if (b_blog.equals(blogText)) {
				System.out.println("Blog Text is same as given.");
			} else {
				System.out.println("Blog Text is not same as given.");
			}
			String b_profession = driver.findElement(
					By.xpath(".//*[@id='collapse0']/div/span[5]")).getText();
			if (b_profession.equalsIgnoreCase(profession)) {
				System.out.println("Profession is same as given.");
			} else {
				System.out.println("Profession is not same as given.");
			}
		} catch (NoSuchElementException e) {
			System.out.println("ERROR: Blog not displayed.");
		} catch (TimeoutException e) {
			System.out.println("ERROR: Occurred in reading blog.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
}
